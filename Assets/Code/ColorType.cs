using UnityEngine;

[CreateAssetMenu()]
public class ColorType : ScriptableObject
{
    public Color myColor;
    public string colorName;
}
