using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    [SerializeField] private float secondsToStartGameOverAnim;
    
    public static GameManager Instance;

    private float shadowProcantage = 1f;

    private Camera mainCamera;

    [SerializeField]
    private ColorType[] allColorTypes;

    [SerializeField]
    private ColorType white;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (Instance == null)
            Instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    private void Start()
    {
        PlayMusic();
        ResetShadoProcent();
    }

    private void OnEnable()
    {
        PillsCounter.OnOverdose += GameOver;
        Pill.OnPlayerTookPill += Show_Hide_Objects;
        MainMenuController.OnGameLoaded += ResetShadoProcent;
    }

    private void OnDisable()
    {
        PillsCounter.OnOverdose -= GameOver;
        Pill.OnPlayerTookPill -= Show_Hide_Objects;
        MainMenuController.OnGameLoaded -= ResetShadoProcent;
    }

    public void SetShadowProcentage(float newShadowProcentage) => this.shadowProcantage = newShadowProcentage;

    public float GetShadowProcentage() => shadowProcantage;

    public void Show_Hide_Objects(ColorType colorType)
    {
        if (!mainCamera) mainCamera = Camera.main;

        mainCamera.cullingMask = (1 << LayerMask.NameToLayer("Default")) |
            (1 << LayerMask.NameToLayer("UI"));

        
        foreach(var color in allColorTypes)
        {
            int layerIndex = LayerMask.NameToLayer(color.colorName);
            bool showColor = color.colorName == "White" || color.colorName == colorType.colorName;
            if (showColor)
                mainCamera.cullingMask = mainCamera.cullingMask | (1 << layerIndex);
        }
    }

    private void PlayMusic() => AudioManager.instance.PlayMainTheme(Consts.GAME_MUSIC);

    private void GameOver()
    {
        ResetShadoProcent();
        SceneManager.LoadScene("GameOver");
    }

    public void ResetShadoProcent()
    {
        PlayerPrefs.SetInt(Consts.NUMBER_OF_TAKEN_PILLS, 0);
        shadowProcantage = 1f;
    }
}
