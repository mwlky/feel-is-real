using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LastPill : MonoBehaviour
{
    public static event Action OnPlayerTookLastPill;

    [SerializeField] private GameObject Canvas;

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("Player")) return;
        OnPlayerTookLastPill?.Invoke();

        Canvas.SetActive(true);
        Destroy(gameObject);
    }
}