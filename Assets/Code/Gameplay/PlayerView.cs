using UnityEngine;

public class PlayerView : MonoBehaviour
{
    [SerializeField]
    private Animator animator;

    [SerializeField]
    private Rigidbody2D rb2d;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    private Vector2 currentVelocity => rb2d.velocity;

    private string SPEED_PARAMETER = "Speed";

    void Update()
    {
        if(Mathf.Abs(currentVelocity.x) > 0f)
            spriteRenderer.flipX = currentVelocity.x < 0.001f;

        animator.SetFloat(SPEED_PARAMETER, currentVelocity.magnitude);
    }
}
