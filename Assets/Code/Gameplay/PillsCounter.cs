using System;
using UnityEngine;

public class PillsCounter : MonoBehaviour
{
    public static event Action OnOverdose;
    
    [SerializeField]
    private int maxNumberOfTakenPills;

    private int numberOfTakenPills;

    public void TakePill()
    {
        numberOfTakenPills = PlayerPrefs.GetInt(Consts.NUMBER_OF_TAKEN_PILLS,0);

        numberOfTakenPills++;

        PlayerPrefs.SetInt(Consts.NUMBER_OF_TAKEN_PILLS, numberOfTakenPills);

        float shadowProcentage = 1f - (float)numberOfTakenPills / maxNumberOfTakenPills;
        GameManager.Instance.SetShadowProcentage(shadowProcentage);

        if (numberOfTakenPills >= maxNumberOfTakenPills)
            OnOverdose?.Invoke();
    }

    public int GetNumberOfPills() => numberOfTakenPills;

    public int GetMaxNumberOfTakenPills() => maxNumberOfTakenPills;
}
