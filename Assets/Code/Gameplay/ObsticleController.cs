using System.Collections;
using UnityEngine;

public class ObsticleController : ColorfulObject
{
    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void Start() => base.Start();

    protected override void SetColor() => base.SetColor();

    protected override void OnValidate() => base.OnValidate();
}
