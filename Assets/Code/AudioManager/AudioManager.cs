﻿using UnityEngine.Audio;
using System;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public Sound[] sounds;

    public static AudioManager instance;

    public AudioMixerGroup musicMixer;
    public AudioMixerGroup soundsMixer;

    private const string MUSICS_VOLUME = "MusicVolume";
    private const string SOUNDS_VOLUME = "SoundsVolume";
    private const float multiplier = 30f;

    void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (instance == null)
            instance = this;
        else
        {
            Destroy(gameObject);
            return;
        }

        for (int i = 0; i < sounds.Length; i++)
        {
            sounds[i].source = gameObject.AddComponent<AudioSource>();
            sounds[i].source.clip = sounds[i].audioClip;

            sounds[i].source.volume = sounds[i].volume;
            sounds[i].source.pitch = sounds[i].pitch;
            sounds[i].source.loop = sounds[i].Loop;
            sounds[i].source.outputAudioMixerGroup = sounds[i].myMixerGroup;
        }
    }

    private void Start()
    {
        SetMusicVolume(PlayerPrefs.GetFloat(MUSICS_VOLUME, 0.7f));
        SetSoundsVolume(PlayerPrefs.GetFloat(SOUNDS_VOLUME, 0.7f));
    }

    public void PauseMusic()
    {
        foreach (Sound sound in sounds)
        {
            sound.source.Pause();
        }
    }

    public void StopMusic()
    {
        foreach (Sound sound in sounds)
        {
            sound.source.Stop();
        }
    }

    public void PlayMainTheme(string clipName)
    {
        PauseMusic();
        PlayClip(clipName);
    }

    private Sound FindSound(string clipName)
    {
        Sound s = Array.Find(sounds, sound => sound.ClipName == clipName);
        if (s == null)
        {
            Debug.Log("I didnt find clip " + clipName);
            return null;
        }
        return s;
    }

    public void PlayClip(string clipName)
    {
        Sound s = FindSound(clipName);

        s.source.Play();
    }

    public void PlayOneShot(string clipName)
    {
        Sound s = FindSound(clipName);

        if(!s.source.isPlaying)
            s.source.PlayOneShot(s.source.clip);
    }

    public void StopPlayingClip(string clipName)
    {
        Sound s = FindSound(clipName);
        s.source.Stop();
    }

    public bool IsPlaying(string clipName)
    {
        Sound s = Array.Find(sounds, sound => sound.ClipName == clipName);
        return s.source.isPlaying;
    }

    public void SetMusicVolume(float value)
    {
        musicMixer.audioMixer.SetFloat(MUSICS_VOLUME, Mathf.Log10(value) * multiplier);
    }

    public void SetSoundsVolume(float value)
    {
        soundsMixer.audioMixer.SetFloat(SOUNDS_VOLUME, Mathf.Log10(value) * multiplier);
    }
}