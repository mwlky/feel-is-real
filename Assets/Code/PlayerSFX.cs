using System.Collections;
using UnityEngine;

public class PlayerSFX : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D rb;

    private Vector2 currentVelocity => rb.velocity;

    [SerializeField]
    private float delayTime = 0.25f;

    private void Update()
    {
        if (currentVelocity.sqrMagnitude > 0.001f)
            PlayStepsSFX();
        else
            StartCoroutine(StopPlayingSFX());
    }

    private void PlayStepsSFX()
    {
        AudioManager.instance.PlayOneShot(Consts.SPEPS_SFX);
    }

    private IEnumerator StopPlayingSFX()
    {
        yield return new WaitForSeconds(delayTime);
        AudioManager.instance.StopPlayingClip(Consts.SPEPS_SFX);
    }
}
