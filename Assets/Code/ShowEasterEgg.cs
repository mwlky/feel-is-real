using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class ShowEasterEgg : MonoBehaviour
{
    [SerializeField] private GameObject EasterEggVideo;
    [SerializeField] private VideoPlayer _videoPlayer;

    public void ShowEasterEggVideo()
    {
        gameObject.SetActive(false);
        EasterEggVideo.SetActive(true);
        _videoPlayer.Play();
    }
}
