using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Portal : ColorfulObject
{
    [SerializeField] private GameObject placeToMove;

    protected override void OnValidate() => base.OnValidate();

    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void SetColor() => base.SetColor();

    protected override void Start() => base.Start();

    private void OnTriggerEnter2D(Collider2D col)
    {
        col.transform.position = placeToMove.transform.position;
        PlaySFX();
    }

    private void PlaySFX() => AudioManager.instance.PlayClip(Consts.PORTAL_SFX);
}
