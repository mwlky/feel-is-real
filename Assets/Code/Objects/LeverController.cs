using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LeverController : ColorfulObject
{
    [SerializeField] private GameObject[] objectsToEnable;

    private void OnTriggerEnter2D(Collider2D col)
    {
        ChangeRotation();
        ChangeEnable();
        PlaySFX();
    }
     
    private void ChangeEnable()
    {
        foreach (var objectToHandle in objectsToEnable)
        {
            objectToHandle.SetActive(!objectToHandle.activeSelf);
        }
    }

    private void ChangeRotation()
    {
        var currentScale = transform.localScale;
        var currentX = currentScale.x;

        var targetX = currentX * -1;

        transform.localScale = new Vector3(targetX, currentScale.y, currentScale.z);
    }

    private void PlaySFX() => AudioManager.instance.PlayClip(Consts.BUTTON_LEVER_SFX);

    protected override void OnValidate() => base.OnValidate();

    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void SetColor() => base.SetColor();

    protected override void Start() => base.Start();
}
