using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChestController : ColorfulObject
{
    [SerializeField]
    private Rigidbody2D rb2d;

    private Vector2 myVelocity => rb2d.velocity;

    protected override void OnValidate() => base.OnValidate();

    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void SetColor() => base.SetColor();

    protected override void Start() => base.Start();

    private void Update()
    {
        if (myVelocity.sqrMagnitude > 0.001f)
            AudioManager.instance.PlayOneShot(Consts.BOX_SFX);
        else
            AudioManager.instance.StopPlayingClip(Consts.BOX_SFX);
    }
}
