using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wall : ColorfulObject
{
    protected override void OnValidate() => base.OnValidate();

    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void SetColor() => base.SetColor();

    protected override void Start() => SetColor();
}