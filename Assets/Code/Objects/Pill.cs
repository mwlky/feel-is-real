using System;
using UnityEngine;

public class Pill : ColorfulObject
{
    public static event Action<ColorType> OnPlayerTookPill;

    protected override void OnEnable() => base.OnDisable();

    protected override void OnDisable() => base.OnDisable();

    protected override void Start() => SetColor();

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("Player")) return;

        ColorType newColorType = col.GetComponent<PlayerColorController>().GetCollorType();

        OnPlayerTookPill?.Invoke(colorType);

        PlaySFX();

        if (newColorType.colorName != "White")
        {
            base.colorType = newColorType;
            SetColor();
        }
        else
            Destroy(gameObject);
    }

    protected override void OnValidate() => base.OnValidate();

    protected override void SetColor() => base.SetColor();

    private void PlaySFX() => AudioManager.instance.PlayClip(Consts.PILL_SFX);
}
