using UnityEngine;

public class ButtonController : ColorfulObject
{
    [SerializeField] private Animator animator;

    [SerializeField] private GameObject[] objectsToControl;

    private bool isPressed = false;

    protected override void OnValidate() => base.OnValidate();

    protected override void OnEnable() => base.OnEnable();

    protected override void OnDisable() => base.OnDisable();

    protected override void SetColor() => base.SetColor();

    private const string IS_BUTTON_PRESSED = "isPressed";

    private float radious = 0.35f;

    protected override void Start()
    {
        base.Start();
        InvokeRepeating("CheckIfPressed", 0f, 0.15f);
    }

    private void Update() => animator.SetBool(IS_BUTTON_PRESSED, isPressed);

    private void CheckIfPressed()
    {
        Collider2D[] colliders;

        if (gameObject.layer == LayerMask.NameToLayer("White"))
            colliders = Physics2D.OverlapCircleAll(transform.position, radious);
        else
            colliders = Physics2D.OverlapCircleAll(transform.position, radious, (1 << gameObject.layer));

        bool wasPressed = isPressed;

        isPressed = colliders.Length > 0;

        if (wasPressed != isPressed)
            PlaySFX();

        foreach (var o in objectsToControl)
        {
            o.SetActive(!isPressed);
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireSphere(transform.position, radious);
    }

    private void PlaySFX() => AudioManager.instance.PlayClip(Consts.BUTTON_LEVER_SFX);
}