using System.Collections;
using UnityEngine;

public abstract class ColorfulObject : MonoBehaviour
{
    [SerializeField]
    protected ColorType colorType;

    [SerializeField]
    protected SpriteRenderer spriteRenderer;

    protected Color myColor => spriteRenderer.color;

    protected Color targetColor;

    protected virtual void OnEnable() => PlayerColorController.OnColorChange += SetColor;

    protected virtual void OnDisable() => PlayerColorController.OnColorChange -= SetColor;

    protected virtual void Start()
    {
        SetColor();

        spriteRenderer.gameObject.layer = LayerMask.NameToLayer(colorType.colorName);
    }

    protected virtual void SetColor()
    {
        GameManager instance = GameManager.Instance;

        if (instance)
        {
            float shadowProcentage = instance.GetShadowProcentage();
            spriteRenderer.color = colorType.myColor;

            targetColor = new Color(myColor.r * shadowProcentage, myColor.g * shadowProcentage,
                myColor.b * shadowProcentage);

            if(gameObject.active)
                StartCoroutine(LerpColor(targetColor));
        }
        else
            spriteRenderer.color = colorType.myColor;
    }

    protected virtual void OnValidate()
    {
        if (!spriteRenderer)
            spriteRenderer = GetComponent<SpriteRenderer>();

        if (!spriteRenderer)
            spriteRenderer.GetComponentInChildren<SpriteRenderer>();

        if (colorType)
            SetColor();
    }

    protected IEnumerator LerpColor(Color newColor)
    {
        float timer = 0f;
        while(myColor != newColor)
        {
            timer += Time.deltaTime / Consts.ANIMATION_TIME;
            spriteRenderer.color = Color.Lerp(colorType.myColor, newColor, timer);
            yield return new WaitForEndOfFrame();
        }
        yield break;
    }
}
