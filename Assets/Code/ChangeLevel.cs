using UnityEngine;
using UnityEngine.SceneManagement;
using System;

public class ChangeLevel : MonoBehaviour
{
    [SerializeField] private string nameLevelToLoad;

    [SerializeField] private Animator animator;

    public static event Action<bool> OnLevelChange;

    private const string FADE_OUT = "FadeOut";

    private void OnTriggerEnter2D(Collider2D col)
    {
        if (!col.CompareTag("Player")) return;

        LoadLevel();
    }

    private void LoadLevel()
    {
        animator.SetTrigger(FADE_OUT);
        OnLevelChange?.Invoke(false);
    }

    public void OnFadeOutHasFinished()
    {
        SceneManager.LoadScene(nameLevelToLoad);
    }
}