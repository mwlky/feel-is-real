using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CheckInput : MonoBehaviour
{
    private bool hasAnimationEnded;

    public void AnimationHasEnded()
    {
        hasAnimationEnded = true;
    }

    private void FixedUpdate()
    {
        if (!(Input.anyKey && hasAnimationEnded)) return;

        SceneManager.LoadScene("MainMenu");
    }
}
