using UnityEngine;
using UnityEngine.SceneManagement;

public class EndGameCanvasController : MonoBehaviour
{
    public void SwitchSceneToBadEnding()
    {
        SceneManager.LoadScene("GameWIn");
    }
}
