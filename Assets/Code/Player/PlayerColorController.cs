using System;
using UnityEngine;

public class PlayerColorController : MonoBehaviour
{
    public static event Action OnColorChange;

    [SerializeField]
    private SpriteRenderer spriteRenderer;

    [SerializeField]
    private PillsCounter pillsCounter;

    [SerializeField]
    private ColorType myColorType;

    private void Start()
    {
        ActualizeColor();
        GameManager.Instance.Show_Hide_Objects(myColorType);
    }

    private void OnEnable()
    {
        Pill.OnPlayerTookPill += SetPlayerCurrentPill;
    }

    public void SetPlayerCurrentPill(ColorType colorType)
    {
        pillsCounter.TakePill();

        myColorType = colorType;
        ActualizeColor();

        gameObject.layer = LayerMask.NameToLayer(colorType.colorName);

        OnColorChange?.Invoke();
    }

    private void OnDisable()
    {
        Pill.OnPlayerTookPill -= SetPlayerCurrentPill;
    }

    public ColorType GetCollorType() => myColorType;

    private void ActualizeColor()
    {
        float shadowProcentage = GameManager.Instance.GetShadowProcentage();
        Color newColor = myColorType.myColor;

        Color currentColor = new Color(newColor.r * shadowProcentage, newColor.g * shadowProcentage,
            newColor.b * shadowProcentage);

        spriteRenderer.color = currentColor;
    }
}
