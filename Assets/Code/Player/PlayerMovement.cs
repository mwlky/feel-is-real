using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] float speed;

    [SerializeField]
    private Rigidbody2D rb;

    private void OnEnable()
    {
        PlayerExitGame.OnHandlePlayerMovement += FreezeMovement;
        ExitGameCanvasController.OnEnablePlayerMovement += FreezeMovement;
        ChangeLevel.OnLevelChange += FreezeMovement;
    }

    private void OnDisable()
    {
        PlayerExitGame.OnHandlePlayerMovement -= FreezeMovement;
        ExitGameCanvasController.OnEnablePlayerMovement -= FreezeMovement;
        ChangeLevel.OnLevelChange -= FreezeMovement;
    }

    private void FixedUpdate()
    {
        float vertical = Input.GetAxis("Vertical");
        float horizontal = Input.GetAxis("Horizontal");

        MovePlayerLeftRight(horizontal, vertical);
    }

    void MovePlayerLeftRight(float horizontal, float vertical)
    {
        rb.velocity = new Vector2(horizontal * speed, vertical * speed);
    }

    void FreezeMovement(bool enabled) => rb.simulated = enabled;
}
