using System;
using UnityEngine;

public class PlayerExitGame : MonoBehaviour
{
    public static event Action<bool> OnHandlePlayerMovement;

    [SerializeField] private GameObject exitGameCanvas;
    private bool isCanvasActive = false;

    private void FixedUpdate()
    {
        if (!Input.GetKey(KeyCode.Escape)) return;

        isCanvasActive = !isCanvasActive;
        
        exitGameCanvas.SetActive(true);
        OnHandlePlayerMovement?.Invoke(false);
    }

}