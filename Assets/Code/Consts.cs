public static class Consts 
{
    public const string MENU_MUSIC = "Menu";
    public const string GAME_MUSIC = "Gameplay";
    public const float ANIMATION_TIME = 0.3f;
    public const string NUMBER_OF_TAKEN_PILLS = "taken pills";
    public const string SPEPS_SFX = "Steps";
    public const string BUTTON_LEVER_SFX = "Button_Lever";
    public const string PILL_SFX = "Pill";
    public const string PORTAL_SFX = "Portal";
    public const string BOX_SFX = "Box";
}
