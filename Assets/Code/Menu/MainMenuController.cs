using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class MainMenuController : MonoBehaviour
{
    public static event Action OnGameLoaded;
    
    [SerializeField]
    private Button playButton;

    [SerializeField]
    private Button settingsButton;

    [SerializeField]
    private Button quitButton;

    [SerializeField]
    private GameObject allButtons;

    [SerializeField]
    private GameObject SettingsPanel;

    private void OnEnable()
    {
        playButton.onClick.AddListener(LoadGame);
//        settingsButton.onClick.AddListener(ShowSettings);
        quitButton.onClick.AddListener(CloseGame);
        SettingsMenu.OnSettingClose += ShowButtons;
    }

    private void OnDisable()
    {
        playButton.onClick.RemoveAllListeners();
 //       settingsButton.onClick.RemoveAllListeners();
        quitButton.onClick.RemoveAllListeners();
        SettingsMenu.OnSettingClose -= ShowButtons;
    }

    private void Start()
    {
        PlayMusic();
        SettingsPanel.SetActive(false);
    }

    private void LoadGame()
    {
        OnGameLoaded?.Invoke();
        SceneManager.LoadScene("Level0");
    }

    private void ShowSettings()
    {
        SettingsPanel.SetActive(true);
        allButtons.SetActive(false);
    }

    private void ShowButtons() => allButtons.SetActive(true);

    private void CloseGame() => Application.Quit();

    private void PlayMusic() => AudioManager.instance.PlayMainTheme(Consts.MENU_MUSIC);
}
