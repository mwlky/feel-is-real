using UnityEngine;
using UnityEngine.UI;
using System;

public class SettingsMenu : MonoBehaviour
{
    [SerializeField]
    Slider musicSlider;

    [SerializeField]
    Slider soundsSlider;

    [SerializeField]
    AudioManager audioManager;

    [SerializeField]
    Button backButton;

    public static event Action OnSettingClose;

    private const string MUSICS_VOLUME = "MusicVolume";
    private const string SOUNDS_VOLUME = "SoundsVolume";

    private void Start()
    {
        musicSlider.value = PlayerPrefs.GetFloat(MUSICS_VOLUME, 0.7f);

        soundsSlider.value = PlayerPrefs.GetFloat(SOUNDS_VOLUME, 0.7f);

        musicSlider.onValueChanged.AddListener(SetMusicVolume);

        soundsSlider.onValueChanged.AddListener(SetSoundsVolume);

        backButton.onClick.AddListener(HideSettings);
    }

    private void OnDisable()
    {
        backButton.onClick.RemoveAllListeners();
        soundsSlider.onValueChanged.RemoveAllListeners();
        musicSlider.onValueChanged.RemoveAllListeners();
        PlayerPrefs.SetFloat(MUSICS_VOLUME, musicSlider.value);
        PlayerPrefs.SetFloat(SOUNDS_VOLUME, soundsSlider.value);
    }

    public void SetMusicVolume(float value)
    {
        audioManager.SetMusicVolume(value);
    }

    public void SetSoundsVolume(float value)
    {
        audioManager.SetSoundsVolume(value);
    }

    public void HideSettings()
    {
        OnSettingClose?.Invoke();
        this.gameObject.SetActive(false);
    }
}
