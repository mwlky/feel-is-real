using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchSpriteOnHover : MonoBehaviour
{
    private Image imageComp;
    private Sprite baseSprite;

    [SerializeField] private Sprite onHoverSprite;

    private void Awake()
    {
        imageComp = GetComponent<Image>();

        baseSprite = imageComp.sprite;
    }

    private void OnMouseEnter()
    {
        Debug.Log("mouse entered");
        imageComp.sprite = onHoverSprite;
    }

    private void OnMouseExit()
    {
        Debug.Log("mouse left");
        imageComp.sprite = baseSprite;
    }
}