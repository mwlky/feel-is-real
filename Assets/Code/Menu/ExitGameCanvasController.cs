using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ExitGameCanvasController : MonoBehaviour
{
    public static event Action<bool> OnEnablePlayerMovement;

    [SerializeField] private Button continueGameButton;
    [SerializeField] private Button exitGameButton;

    private void OnEnable()
    {
        continueGameButton.onClick.AddListener(ContinueGame);
        exitGameButton.onClick.AddListener(ExitGame);
    }

    private void ContinueGame()
    {
        OnEnablePlayerMovement?.Invoke(true);
        gameObject.SetActive(false);
    }

    private void ExitGame() => Application.Quit();

    private void OnDisable()
    {
        continueGameButton.onClick.RemoveListener(ContinueGame);
        exitGameButton.onClick.RemoveListener(ExitGame);
    }
}
