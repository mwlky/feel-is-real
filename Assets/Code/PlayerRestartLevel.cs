using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PlayerRestartLevel : MonoBehaviour
{
    private void Update()
    {
        if (!Input.GetKey(KeyCode.Space)) return;

        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
}
