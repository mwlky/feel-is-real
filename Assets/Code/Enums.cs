using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public enum PillsType
{
    Null,
    Blue,
    Red,
    Green,
    Yellow,
    Purple,
}